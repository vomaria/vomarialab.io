

src/assets/icons/icon-%.png: src/assets/icons/icon-512x512.png
	convert $< -resize $* $@


icons: src/assets/icons/icon-72x72.png src/assets/icons/icon-96x96.png src/assets/icons/icon-128x128.png src/assets/icons/icon-144x144.png src/assets/icons/icon-152x152.png src/assets/icons/icon-192x192.png src/assets/icons/icon-384x384.png
