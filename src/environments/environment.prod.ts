export const environment = {
  production: true,
  ga: 'G-VPRG43RNK1',
  pix: {
    key: '31873717000171',
    owner: 'Ronaldo Lucas Beluque',
    city: 'Marília',
  },
  whatsapp: '+5514996790330',
};
