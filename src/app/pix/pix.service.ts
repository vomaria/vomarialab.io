import {
  Inject,
  Injectable,
  InjectionToken,
  ModuleWithProviders,
  NgModule,
} from '@angular/core';
import { createStaticPix } from 'pix-utils';

export interface Options {
  key: string;
  owner: string;
  city: string;
}

const DEFAULT_OPTIONS: Options = {
  key: '40289086809',
  owner: 'Ettore',
  city: 'Marília',
};

const PIX_TOKEN = new InjectionToken<Options>('PIX_TOKEN');

@Injectable()
export class PixService {
  constructor(
    @Inject(PIX_TOKEN)
    private options: Options
  ) {}

  create(value: number): string {
    const pix =  createStaticPix({
      merchantName: this.options.owner,
      merchantCity: this.options.city,
      pixKey: this.options.key,
      transactionAmount: value,
      txid: '',
    }) as any;
    return pix['toBRCode']();
  }
}

@NgModule()
export class PixModule {
  static forRoot(options: Partial<Options>): ModuleWithProviders<PixModule> {
    const finalOptions = { ...DEFAULT_OPTIONS, ...options };
    return {
      ngModule: PixModule,
      providers: [{ provide: PIX_TOKEN, useValue: finalOptions }, PixService],
    };
  }
}
