import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { map, mergeMap, Observable, tap } from 'rxjs';
import { Product } from '.';
import { ProductService } from './product.service';

@Component({
  selector: 'app-product-page',
  template: `
    <section
      class="container"
      *ngIf="product$ | async as product; else loading"
    >
      <app-product [product]="product">
        <app-product-options-table [product]="product"></app-product-options-table>
      </app-product>
    </section>
    <ng-template #loading>
      <div class="col-12 text-center">
        <div class="spinner-border" role="status">
          <span class="visually-hidden">Loading...</span>
        </div>
      </div>
    </ng-template>
  `,
  styles: [],
})
export class ProductPage {
  constructor(private route: ActivatedRoute, private service: ProductService, private title: Title) {}

  public get product$(): Observable<Product | undefined> {
    return this.route.params.pipe(
      map((params) => params['productSlug']),
      mergeMap((slug: string) => this.service.getOneBySlug(slug)),
      tap( product => this.title.setTitle(['Vó Maria', product!!.name].join(' | '))),
    );
  }
}
