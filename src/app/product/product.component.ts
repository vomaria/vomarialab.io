import { Component, Input, OnInit } from '@angular/core';
import { Nulls, Product } from '../product';

@Component({
  selector: 'app-product',
  styles: [
    `
      ul {
        list-style-type: none;
        padding: 0;
        margin: 0;
      }
    `,
  ],
  template: `
    <div class="card">
      <div class="card-header">
        <a [routerLink]="['/', product.slug]"
          ><h1 class="title">{{ product.name }}</h1></a
        >
        <p *ngIf="product.description">{{ product.description }}</p>
      </div>
      <div class="card-body row">
        <ngb-carousel
          *ngIf="product.pictures"
          class="col-md-6 col-12"
          [showNavigationArrows]="product.pictures.length > 1"
          [showNavigationIndicators]="product.pictures.length > 1"
        >
          <ng-template ngbSlide *ngFor="let picture of product.pictures">
            <div class="picsum-img-wrapper">
              <img
                class="d-block w-100"
                [src]="picture.url"
                alt="{{ product.name }}"
              />
            </div>
          </ng-template>
        </ngb-carousel>
        <div class="col-md-6 col-12">
          <ng-content></ng-content>
        </div>
      </div>
    </div>
  `,
})
export class ProductComponent implements OnInit {
  @Input()
  public product: Product = Nulls.PRODUCT;

  constructor() {}

  ngOnInit(): void {}
}
