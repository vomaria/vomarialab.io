import { Component, Input, OnInit } from '@angular/core';
import { Nulls, Product, ProductOption } from '.';
import { WhatsappService } from '../whatsapp/whatsapp.service';

@Component({
  selector: 'app-product-options-table',
  template: `
    <table class="table table-striped table-hover">
      <tbody>
        <tr *ngFor="let option of product.options">
          <td>{{ option.quantity | number: '1.0' }}{{ option.unit }}</td>
          <td>{{ option.price | currency: 'R$':true:'1.2-2':'pt_br' }}</td>
          <td class="text-end">
            <a
              class="btn btn-primary"
              [routerLink]="['/', product.slug, option.slug]"
              >Mais</a
            >
            <a
              class="btn btn-success"
              target="_blank"
              [href]="buyNowLink(option)"
              >Comprar Agora</a
            >
          </td>
        </tr>
      </tbody>
    </table>
  `,
  styles: [],
})
export class ProductOptionsTable implements OnInit {
  @Input()
  public product: Product = Nulls.PRODUCT;

  ngOnInit(): void {}

  constructor(private whatsapp: WhatsappService) {}

  buyNowLink(option: ProductOption): string {
    return this.whatsapp.createLink(this.product, option);
  }
}
