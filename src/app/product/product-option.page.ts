import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { map, mergeMap, Observable, tap } from 'rxjs';
import { Product, ProductOption } from '.';
import { ProductService } from './product.service';

@Component({
  selector: 'app-product-option-page',
  template: `
    <section class="container" *ngIf="option$ | async as option; else loading">
      <app-product [product]="option.product">
        <app-product-option
          [product]="option.product"
          [option]="option"
        ></app-product-option>
      </app-product>
    </section>
    <ng-template #loading>
      <div class="col-12 text-center">
        <div class="spinner-border" role="status">
          <span class="visually-hidden">Loading...</span>
        </div>
      </div>
    </ng-template>
  `,
  styles: [],
})
export class ProductOptionPage implements OnInit {
  constructor(private route: ActivatedRoute, private service: ProductService, private title: Title) {}

  public get product$(): Observable<Product | undefined> {
    return this.route.params.pipe(
      map((params) => params['productSlug']),
      mergeMap((slug: string) => this.service.getOneBySlug(slug))
    );
  }

  public get option$(): Observable<Required<ProductOption> | undefined> {
    return this.route.params.pipe(
      mergeMap((params) =>
        this.service.getOptionBySlugs(
          params['productSlug'],
          params['optionSlug']
        ),
      ),
      tap( option => this.title.setTitle(['Vó Maria', option!!.product.name, option?.slug].join(' | '))),
    );
  }

  ngOnInit(): void {}
}
