import { Inject, Injectable } from '@angular/core';
import { find, from, map, Observable, of } from 'rxjs';
import { Product, ProductOption, ProductProvider } from '.';
import { PROVIDER_TOKEN } from './product.module';

class StaticProvider implements ProductProvider {
  constructor(private products: Product[]) {}

  getAll(): Observable<Product[]> {
    return of(this.products);
  }

  getOneBySlug(slug: string): Observable<Product | undefined> {
    return from(this.products).pipe(find((it) => it.slug == slug));
  }
}

export class ProviderFactory {
  static createStatic(products: Product[]) {
    return new StaticProvider(products);
  }
}

@Injectable()
export class ProductService implements ProductProvider {
  constructor(
    @Inject(PROVIDER_TOKEN)
    private provider: ProductProvider
  ) {}
  getAll(): Observable<Product[]> {
    return this.provider.getAll();
  }
  getOneBySlug(slug: string): Observable<Product | undefined> {
    return this.provider.getOneBySlug(slug);
  }
  getOptionBySlugs(
    productSlug: string,
    optionSlug: string
  ): Observable<Required<ProductOption> | undefined> {
    return this.getOneBySlug(productSlug).pipe(
      map((product) => {
        return Object.assign(
          product?.options.find((it) => it.slug == optionSlug) as ProductOption,
          { product }
        ) as any;
      })
    );
  }
}
