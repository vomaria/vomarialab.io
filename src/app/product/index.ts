import { Observable } from 'rxjs';

export interface Picture {
  url: string;
}

export interface ProductOption {
  slug: string;
  price: number;
  quantity: number;
  unit: string;
  product?: Product;
}

export interface MinimalProductOption {
  price: number;
  quantity: number;
  unit: string;
}

export interface Product {
  slug: string;
  name: string;
  description?: string;
  pictures: Picture[];
  options: ProductOption[];
}

export interface MinimalProduct {
  name: string;
  description?: string;
  pictures: Picture[];
  options: MinimalProductOption[];
}

export function prepareOption(option: MinimalProductOption): ProductOption {
  return {
    ...option,
    slug: `${option.quantity}${option.unit}`,
  };
}

export function prepareProduct(product: MinimalProduct): Product {
  return {
    ...product,
    options: product.options.map(prepareOption),
    slug: product.name
      .normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '')
      .replace(/ /gi, '-')
      .replace(/-+/gi, '-')
      .toLocaleLowerCase(),
  };
}

export function prepareProducts(products: MinimalProduct[]): Product[] {
  return products.map(prepareProduct);
}

export const Nulls = {
  PRODUCT: {
    slug: 'null',
    name: 'null',
    description: 'null',
    options: [],
    pictures: [],
  },
  OPTION: {
    slug: 'null',
    price: 0,
    quantity: 0,
    unit: '?',
  },
};

export interface ProductProvider {
  getAll(): Observable<Product[]>;
  getOneBySlug(slug: string): Observable<Product | undefined>;
}
