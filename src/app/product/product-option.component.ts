import { Component, Input, OnInit } from '@angular/core';
import { Nulls, Product, ProductOption } from '.';
import { PixService } from '../pix/pix.service';
import { WhatsappService } from '../whatsapp/whatsapp.service';

@Component({
  selector: 'app-product-option',
  template: `
    <div class="row">
      <h3 class="col-12 col-lg-6 text-center">
        {{ option.quantity | number: '1.0' }}{{ option.unit }}
        {{ option.price | currency: 'R$':true:'1.2-2':'pt-BR' }}
      </h3>
      <div class="col-12 col-lg-6 text-center">
        <button
          type="button"
          class="btn btn-primary"
          type="button"
          (click)="copyPix()"
        >
          PIX Copia e Cola
        </button>
        <a
          target="_blank"
          [href]="buyNowLink()"
          class="btn btn-success"
          type="button"
          >Comprar Agora</a
        >
      </div>
    </div>
    <div class="col-12 text-center" *ngIf="createPix() as pixCode">
      <qrcode [qrdata]="pixCode" [width]="256" [errorCorrectionLevel]="'M'">
      </qrcode>
    </div>
  `,
  styles: [],
})
export class ProductOptionComponent implements OnInit {
  @Input()
  public product: Product = Nulls.PRODUCT;

  @Input()
  public option: ProductOption = Nulls.OPTION;

  constructor(private whatsapp: WhatsappService, private pix: PixService) {}

  ngOnInit(): void {}

  buyNowLink(): string {
    return this.whatsapp.createLink(this.product, this.option);
  }

  createPix(): string {
    return this.pix.create(this.option.price);
  }

  copyPix(): void {
    navigator.clipboard
      .writeText(this.createPix())
      .then(() => alert('Copiado para área de transferência'))
      .catch(console.error);
  }
}
