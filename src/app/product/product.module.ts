import { InjectionToken, ModuleWithProviders, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { QRCodeModule } from 'angularx-qrcode';
import { ProductProvider, Product } from '.';
import { ProductOptionComponent } from './product-option.component';
import { ProductOptionPage } from './product-option.page';
import { ProductOptionsTable } from './product-options.table';
import { ProductComponent } from './product.component';
import { ProductPage } from './product.page';
import { ProductService, ProviderFactory } from './product.service';

export interface Options {
  products: Product[];
}

export const OPTIONS_TOKEN = new InjectionToken<Options>('PRODUCT_OPTIONS');

export const PROVIDER_TOKEN = new InjectionToken<ProductProvider>(
  'PRODUCT_PROVIDER'
);

export const DEFAULT_OPTIONS: Options = {
  products: [],
};

const COMPONENTS = [
  ProductComponent,
  ProductPage,
  ProductOptionComponent,
  ProductOptionPage,
  ProductOptionsTable,
];

@NgModule({
  declarations: COMPONENTS,
  imports: [BrowserModule, QRCodeModule, RouterModule, NgbModule],
  exports: COMPONENTS,
})
export class ProductModule {
  static forRoot(
    options: Partial<Options>
  ): ModuleWithProviders<ProductModule> {
    const finalOptions = { ...DEFAULT_OPTIONS, ...options };
    return {
      ngModule: ProductModule,
      providers: [
        ProductService,
        {
          provide: PROVIDER_TOKEN,
          useValue: ProviderFactory.createStatic(finalOptions.products),
        },
        {
          provide: OPTIONS_TOKEN,
          useValue: finalOptions,
        },
      ],
    };
  }
}
