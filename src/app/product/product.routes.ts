import { Routes } from '@angular/router';
import { ProductOptionPage } from './product-option.page';
import { ProductPage } from './product.page';

export const routes: Routes = [
  { path: ':productSlug', component: ProductPage, pathMatch: 'full' },
  {
    path: ':productSlug/:optionSlug',
    component: ProductOptionPage,
    pathMatch: 'full',
  },
];
