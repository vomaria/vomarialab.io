import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { Product } from '../product';
import { ProductService } from '../product/product.service';

@Component({
  selector: 'app-home',
  template: `
    <section
      class="container"
      *ngIf="products$ | async as products; else loading"
    >
      <ul>
        <li *ngFor="let product of products">
          <app-product [product]="product">
            <app-product-options-table
              [product]="product"
            ></app-product-options-table>
          </app-product>
        </li>
      </ul>
    </section>
    <ng-template #loading>Loading...</ng-template>
  `,
  styles: [
    `
      ul {
        list-style-type: none;
        padding: 0;
        margin: 0;
      }
    `,
  ],
})
export class HomeComponent implements OnInit {
  constructor(private productService: ProductService, private title: Title) {}

  ngOnInit(): void {
    this.title.setTitle('Vó Maria');
  }

  get products$(): Observable<Product[]> {
    return this.productService.getAll();
  }
}
