import { prepareProducts, Product } from './product';

export const PRODUCTS: Product[] = prepareProducts([
  {
    name: 'Amêndoa',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2016/10/25/12/56/almonds-1768792_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 15,
        quantity: 200,
      },
    ],
  },
  {
    name: 'Aveia em Flocos',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2022/02/07/08/55/oats-6998869_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 6,
        quantity: 500,
      },
    ],
  },
  {
    name: 'Aveia em Farelo',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2019/09/30/22/04/oatmeal-4516834_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 7,
        quantity: 500,
      },
    ],
  },
  {
    name: 'Castanha de Caju',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2018/07/08/08/44/anacardium-3523449_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 25,
        quantity: 300,
      },
      {
        unit: 'g',
        price: 17,
        quantity: 200,
      },
    ],
  },
  {
    name: 'Castanha do Pará',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2021/01/10/18/44/nuts-5906063_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 23,
        quantity: 300,
      },
      {
        unit: 'g',
        price: 15,
        quantity: 200,
      },
    ],
  },
  {
    name: 'Damasco',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2018/04/21/12/28/dried-apricots-3338362_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 22,
        quantity: 300,
      },
    ],
  },
  {
    name: 'Granola Tradicional',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2016/02/18/16/39/granola-1207538_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 20,
        quantity: 500,
      },
    ],
  },
  {
    name: 'Granola Zero com Frutas',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2022/02/02/20/57/granola-6989374_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 15,
        quantity: 500,
      },
    ],
  },
  {
    name: 'Granola Zero Tradicional',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2022/02/02/20/57/granola-6989372_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 15,
        quantity: 500,
      },
    ],
  },
  {
    name: 'Mix de Castanhas',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2020/09/08/16/49/nuts-5555299_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 20,
        quantity: 300,
      },
      {
        unit: 'g',
        price: 15,
        quantity: 200,
      },
    ],
  },
  {
    name: 'Mix de Frutas Desidratadas',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2015/03/30/19/36/dried-fruit-700015_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 20,
        quantity: 250,
      },
    ],
  },
  {
    name: 'Macadamia',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2016/01/21/06/38/australian-1152861_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 22,
        quantity: 200,
      },
    ],
  },
  {
    name: 'Noz Pecan',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2017/05/14/16/52/walnuts-2312506_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 22,
        quantity: 200,
      },
    ],
  },
  {
    name: 'Tâmara',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2021/09/19/19/18/dates-6638825_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 15,
        quantity: 300,
      },
    ],
  },
  // amendoins
  {
    name: 'Amendoim Crocante Cebola e Salsa',
    //description: 'teste',
    pictures: [
      {
        url: '/assets/logo.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 5,
        quantity: 200,
      },
    ],
  },
  {
    name: 'Amendoim Crocante Churrasco',
    //description: 'teste',
    pictures: [
      {
        url: '/assets/logo.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 5,
        quantity: 200,
      },
    ],
  },
  {
    name: 'Amendoim Japonês',
    //description: 'teste',
    pictures: [
      {
        url: '/assets/logo.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 5,
        quantity: 200,
      },
    ],
  },
  {
    name: 'Amendoim Crocante Pimenta',
    //description: 'teste',
    pictures: [
      {
        url: '/assets/logo.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 5,
        quantity: 200,
      },
    ],
  },
  {
    name: 'Amendoim Tradicional',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2015/05/23/13/12/peanuts-780531_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 5,
        quantity: 200,
      },
    ],
  },
  {
    name: 'Amendoim sem pele com alho',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2015/03/11/07/28/peanut-668283_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 7,
        quantity: 200,
      },
    ],
  },
  {
    name: 'Amendoim sem pele com Limão e Pimenta',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2017/12/22/13/58/peanuts-3033680_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 7,
        quantity: 200,
      },
    ],
  },
  {
    name: 'Biscoito de Polvilho - Alho',
    //description: 'teste',
    pictures: [
      {
        url: '/assets/logo.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 10,
        quantity: 200,
      },
    ],
  },
  {
    name: 'Biscoito de Polvilho - Cebola',
    //description: 'teste',
    pictures: [
      {
        url: '/assets/logo.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 10,
        quantity: 200,
      },
    ],
  },
  {
    name: 'Biscoito de Polvilho - Chia',
    //description: 'teste',
    pictures: [
      {
        url: '/assets/logo.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 10,
        quantity: 200,
      },
    ],
  },
  {
    name: 'Biscoito de Polvilho - Parmesão',
    //description: 'teste',
    pictures: [
      {
        url: '/assets/logo.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 10,
        quantity: 200,
      },
    ],
  },
  {
    name: 'Tempero Ana Maria',
    //description: 'teste',
    pictures: [
      {
        url: '/assets/logo.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 7,
        quantity: 80,
      },
    ],
  },
  {
    name: 'Aginomoto',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2013/03/08/14/20/salt-91539_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 5,
        quantity: 100,
      },
    ],
  },
  {
    name: 'Açafrão',
    //description: 'teste',
    pictures: [
      {
        url: '/assets/logo.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 4,
        quantity: 100,
      },
    ],
  },
  {
    name: 'Chimichurri Tradicional	',
    //description: 'teste',
    pictures: [
      {
        url: '/assets/logo.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 6,
        quantity: 60,
      },
    ],
  },
  {
    name: 'Chimichurri Defumado	',
    //description: 'teste',
    pictures: [
      {
        url: '/assets/logo.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 6,
        quantity: 60,
      },
    ],
  },
  {
    name: 'Tempero Edu Guedes',
    //description: 'teste',
    pictures: [
      {
        url: '/assets/logo.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 6,
        quantity: 80,
      },
    ],
  },
  {
    name: 'Paprica Doce',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2016/02/17/19/22/clear-1205688_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 3,
        quantity: 100,
      },
    ],
  },
  {
    name: 'Lemon Pepper',
    //description: 'teste',
    pictures: [
      {
        url: '/assets/logo.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 8,
        quantity: 130,
      },
    ],
  },
  {
    name: 'sal do Himalaia',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2021/09/14/14/17/himalayan-salt-6624128_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 8,
        quantity: 130,
      },
    ],
  },
  {
    name: 'Poupa de Açai em Pó',
    //description: 'teste',
    pictures: [
      {
        url: '/assets/logo.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 20,
        quantity: 200,
      },
      {
        unit: 'g',
        price: 10,
        quantity: 100,
      },
    ],
  },
  {
    name: 'Canela em Pó',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2018/12/04/23/27/cinnamon-3856840_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 6,
        quantity: 100,
      },
    ],
  },
  {
    name: 'Semente de Linhaça',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2014/04/05/11/39/linseed-316590_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 6,
        quantity: 300,
      },
    ],
  },
  {
    name: 'Semente de Chia',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2017/03/05/20/24/chia-2119771_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 20,
        quantity: 300,
      },
    ],
  },
  {
    name: 'Quinoa Branca em Grãos',
    //description: 'teste',
    pictures: [
      {
        url: 'https://cdn.pixabay.com/photo/2021/06/16/14/31/quinoa-6341423_960_720.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 5,
        quantity: 200,
      },
    ],
  },
  {
    name: 'Quinoa Branca em Flocos',
    //description: 'teste',
    pictures: [
      {
        url: '/assets/logo.jpg',
      },
    ],
    options: [
      {
        unit: 'g',
        price: 10,
        quantity: 300,
      },
    ],
  },
]);
