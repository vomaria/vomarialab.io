import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { LOCALE_ID } from '@angular/core';
import { ServiceWorkerModule } from '@angular/service-worker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {
  NgxGoogleAnalyticsModule,
  NgxGoogleAnalyticsRouterModule
} from 'ngx-google-analytics';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PRODUCTS } from './data';
import { FrontModule } from './front/front.module';
import { HomeComponent } from './home/home.component';
import { PixModule } from './pix/pix.service';
import { ProductModule } from './product/product.module';
import { WhatsappModule } from './whatsapp/whatsapp.service';
registerLocaleData(localePt);

@NgModule({
  declarations: [AppComponent, HomeComponent],
  providers: [{ provide: LOCALE_ID, useValue: 'pt-BR' }],
  imports: [
    BrowserModule,
    NgxGoogleAnalyticsModule.forRoot(environment.ga),
    NgxGoogleAnalyticsRouterModule,
    AppRoutingModule,
    FrontModule,
    ProductModule.forRoot({
      products: PRODUCTS,
    }),
    WhatsappModule.forRoot({ number: environment.whatsapp }),
    PixModule.forRoot(environment.pix),
    NgbModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000',
    }),
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
