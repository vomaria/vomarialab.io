import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  template: `
    <!-- Footer -->
    <footer class="text-center">
      <!-- Grid container -->
      <div class="container p-4">
        <!-- Section: Social media -->
        <section class="mb-4">
          <!-- Instagram -->
          <a
            class="btn btn-outline-light btn-floating m-1"
            href="https://www.instagram.com/emporio.vomaria_prod.naturais/"
            role="button"
            ><i class="fa fa-instagram"></i
          ></a>
        </section>
        <!-- Section: Social media -->

        <!-- Section: Text -->
        <section class="mb-4">
          <p>Produtos naturais com a qualidade que toda vó presa! 😍</p>
        </section>
        <!-- Section: Text -->
      </div>
      <!-- Grid container -->

      <!-- Copyright -->
      <div class="text-center p-3">© 2022 Copyright: Empório Vó Maria</div>
      <!-- Copyright -->
    </footer>
    <!-- Footer -->
  `,
  styles: [
    `
      footer {
        background-color: #58b94a;
        color: #553820;
      }
    `,
  ],
})
export class FooterComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
