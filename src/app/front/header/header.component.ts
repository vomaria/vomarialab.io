import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  template: `
    <header>
      <nav class="navbar navbar-expand-lg navbar-light fixed-top">
        <div class="container-fluid">
          <a class="navbar-brand text-light" href="#">
            <img
              src="assets/logo.png"
              alt="Empório Vó Maria"
              class="d-inline-block align-text-top"
            />
          </a>
        </div>
      </nav>
    </header>
  `,
  styles: [
    `
      ::ng-deep body {
        padding-top: 70px;
      }

      header > nav.navbar {
        background-color: #58b94a;
        color: white;
      }

      header * > a.navbar-brand {
        padding: 0;
        margin-top: -4px;
        margin-bottom: -4px;

        img {
          height: 48px;
        }
      }
    `,
  ],
})
export class HeaderComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
