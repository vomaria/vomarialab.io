import {
  Inject,
  Injectable,
  InjectionToken,
  ModuleWithProviders,
  NgModule,
} from '@angular/core';
import { Product, ProductOption } from '../product';

export interface Options {
  number: string;
  api: string;
}

const WHATSAPP_TOKEN = new InjectionToken<Options>('WHATSAPP_TOKEN');

const DEFAULT_OPTIONS: Options = {
  api: 'https://api.whatsapp.com/',
  number: '',
};

export class Options {
  constructor(
    public number: string,
    public api: string = 'https://api.whatsapp.com/'
  ) {}
}

@Injectable()
export class WhatsappService {
  constructor(
    @Inject(WHATSAPP_TOKEN)
    private options: Options
  ) {}

  createLink(product: Product, option: ProductOption) {
    const text = `
      Olá, gostaria de comprar ${option.quantity}${option.unit} de ${product.name}.
    `;
    return this.createSendLink(text, this.options.number);
  }

  createSendLink(text: string, phone: string) {
    const params = Object.entries({
      text,
      phone,
    })
      .map((kv) => kv.map(encodeURIComponent).join('='))
      .join('&');
    return `${this.options.api}send?${params}`;
  }
}

@NgModule()
export class WhatsappModule {
  static forRoot(
    options: Partial<Options>
  ): ModuleWithProviders<WhatsappModule> {
    const finalOptions: Options = { ...DEFAULT_OPTIONS, ...options };
    return {
      ngModule: WhatsappModule,
      providers: [
        { provide: WHATSAPP_TOKEN, useValue: finalOptions },
        WhatsappService,
      ],
    };
  }
}
